# ARDUINO NANO TEST BOARD

The purpose of this page is to explain step by step the realization of a test and an experimentation board based on ARDUINO NANO.

The board uses the following components :

 * an ARDUINO NANO
 * some passive components
 * the board is powered by the USB or by two 1860 LITHIUM-ION batteries.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/05/carte-de-test-arduino-nano.html