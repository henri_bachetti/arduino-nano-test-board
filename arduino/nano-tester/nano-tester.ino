/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the Uno and
  Leonardo, it is attached to digital pin 13. If you're unsure what
  pin the on-board LED is connected to on your Arduino model, check
  the documentation at http://www.arduino.cc

  This example code is in the public domain.

  modified 8 May 2014
  
  by Scott Fitzgerald
 */


// the setup function runs once when you press reset or power the board
void setup()
{
  Serial.begin(115200);
  for (int i = 2 ; i < 14 ; i++) {
    pinMode(i, OUTPUT);
  }
  delay(2000);
}

// the loop function runs over and over again forever
void loop() {
  analogRead(6);
  int a6 = analogRead(6);
  analogRead(7);
  int a7 = analogRead(7);
  Serial.print("A6 : ");
  Serial.println(a6);
  Serial.print("A7 : ");
  Serial.println(a7);
  for (int i = A0 ; i <= A7 ; i++) {
    analogRead(i);
    int a = analogRead(i);
    Serial.print("Analog ");
    Serial.print("A");
    Serial.print(i-14);
    Serial.print(": ");
    Serial.println(a);
    delay(200);
  }
  for (int i = 2 ; i < 14 ; i++) {
    analogRead(6);
    a6 = analogRead(6);
    analogRead(7);
    a7 = analogRead(7);
    if (a6 < 710 && a6 > 680) {
      a6 = a7 = 50;
    }
    digitalWrite(i, LOW);    // turn the LED off by making the voltage LOW
    delay(a6);
    digitalWrite(i, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(a7);
  }
}
